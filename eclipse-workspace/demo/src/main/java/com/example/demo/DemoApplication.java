package com.example.demo;

import javax.transaction.Transactional;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.domains.contract.repositories.ActorRepository;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Autowired
	Persona persona;
	
	@Autowired
	ActorRepository dao;
	
	@Autowired
	private Validator validator;
	
	@Override
	@Transactional
	public void run(String... args) throws Exception {
		persona.saluda();
		System.out.println("Hola mundooo");
		persona.despide();
		
		dao.findAll().forEach(item -> System.out.println(item));
		// var actor = dao.findById(1).get();
		// actor.setFirstName("p");
		
		
		// var constraintViolation = validator.validate(actor);
		
		// if (constraintViolation.size() > 0) {
		// 	constraintViolation.forEach(item -> System.out.println(item.getMessage()));
		// } else {
		// 	dao.save(actor);
		// 	
		// }
		
		// actor.get().getFilmActors().forEach(item -> System.out.println(item.getFilm().getTitle()));
		// dao.findByActorIdBetween(50, 100).forEach(item -> System.out.println(item));
		// dao.findTopNum5ByFirstNameStartingWith("p").forEach(item -> System.out.println(item));
		// dao.findByActorIdBetweenAndLastNameEndingWith(50, 100, "a").forEach(item -> System.out.println(item));
		
	}

}
