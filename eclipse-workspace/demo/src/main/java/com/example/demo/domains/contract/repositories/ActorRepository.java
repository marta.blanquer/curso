package com.example.demo.domains.contract.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.domains.entities.Actor;

public interface ActorRepository extends JpaRepository<Actor, Integer> {
	
	List<Actor> findTopNum5ByFirstNameStartingWith(String nombre);
	List<Actor> findByActorIdBetween(int idInicial, int idFinal);
	List<Actor> findByActorIdBetweenAndLastNameEndingWith(int idInicial, int idFinal, String letraFinal);

}
