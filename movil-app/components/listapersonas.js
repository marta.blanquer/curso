import React from "react";
import { FlatList, StyleSheet, Text, View, Image } from "react-native";
import personas from "../data/personas"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

const ListaPersonas = () => {
  return (
    <View style={styles.container}>
      <FlatList
        data={personas}
        renderItem={({ item }) => (
          <View style={{ flex: 1 }}>
            <Text style={styles.item}>
              {item.tratamiento +
                " " +
                item.nombre +
                " " +
                item.apellidos +
                (item.email ? (" " + item.email) : "")}
            </Text>
            {item.avatar ?  (
              <Image
                style={{ height: 40, width: 40 }}
                source={{
                  uri: item.avatar,
                }}
              ></Image>
            ): null}
          </View>
        )}
      />
    </View>
  );
}

export default ListaPersonas;