import React, { useState } from "react";
import { StyleSheet, Text, View, Button, Alert } from "react-native";

export default function Contador(props) {
  const [count, setCount] = useState(props.init ? props.init : 0);

  return (
    <View style={{ alignItems: "center" }}>
      <Text>Contador: {count}</Text>
      <View style={{ flexDirection: "row" }}>
        <Button title={"+"} onPress={() => setCount(count + 1)} />
        <Button title={"-"} onPress={() => setCount(count - 1)} />
      </View>
    </View>
  );
}
