import React, { useState } from "react";
import { StyleSheet, Text, View, Alert, TouchableOpacity } from "react-native";

export default function Calculadora(props) {
  const [pantalla, setPantalla] = useState(
    props && props.init ? props.init : "0"
  );

  return (
    <View>
      <View style={estiloCalculadora.titulo}>
        <Text style={estiloCalculadora.titulo}>La Calculadora</Text>
      </View>
      <View>
        <View style={estiloCalculadora.pantalla}>
          <Text style={estiloCalculadora.textoPantalla}>{pantalla}</Text>
        </View>
        <View style={estiloCalculadora.controles}>
          <View style={estiloCalculadora.filas}>
            <AppButton
              title={"0"}
              onPress={() => setPantalla(pantalla + "0")}
            />
            <AppButton
              title={"1"}
              onPress={() => setPantalla(pantalla + "1")}
            />
            <AppButton
              title={"2"}
              onPress={() => setPantalla(pantalla + "2")}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <AppButton
              title={"3"}
              onPress={() => setPantalla(pantalla + "3")}
            />
            <AppButton
              title={"4"}
              onPress={() => setPantalla(pantalla + "4")}
            />
            <AppButton
              title={"5"}
              onPress={() => setPantalla(pantalla + "5")}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <AppButton
              title={"6"}
              onPress={() => setPantalla(pantalla + "6")}
            />
            <AppButton
              title={"7"}
              onPress={() => setPantalla(pantalla + "7")}
            />
            <AppButton
              title={"8"}
              onPress={() => setPantalla(pantalla + "8")}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <AppButton
              title={"9"}
              onPress={() => setPantalla(pantalla + "9")}
            />
            <AppButton
              title={"+"}
              onPress={() => setPantalla(pantalla + "+")}
            />
            <AppButton
              title={"-"}
              onPress={() => setPantalla(pantalla + "-")}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <AppButton
              title={"/"}
              onPress={() => setPantalla(pantalla + "/")}
            />
            <AppButton
              title={"*"}
              onPress={() => setPantalla(pantalla + "*")}
            />
            <AppButton
              title={"."}
              onPress={() => setPantalla(pantalla + ".")}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <AppButton
              ancho={120}
              title={"C"}
              onPress={() => setPantalla("")}
            />
            <AppButton
              title={"="}
              onPress={() => setPantalla(eval(pantalla))}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const estiloCalculadora = StyleSheet.create({
  titulo: {
    fontSize: 30,
  },
  pantalla: {
    borderWidth: 1,
    borderColor: "black",
    width: 200,
    height: 40,
  },
  textoPantalla: {
    fontSize: 30,
  },
  controles: {
    width: 200,
  },
  filas: {
    flexDirection: "row",
    width: 100,
  },
});

const AppButton = ({ onPress, title, ancho }) => (
  <TouchableOpacity
    onPress={onPress}
    style={[styles.appButtonContainer, { width: ancho ? ancho : 60 }]}
  >
    <Text style={styles.appButtonText}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    width: 60,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase",
  },
});
