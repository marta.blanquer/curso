import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Button,
  Alert,
} from "react-native";
import Example, { Contador } from "./contador";
import ListaPersonas from "./listapersonas";
import Calculadora from "./calculadora";
import Check from "./check";
import ModalView from "./modal";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
const Stack = createNativeStackNavigator();

export default function Demos({ navigation }) {
  const createTwoButtonAlert = () =>
    Alert.alert("Alertaaa!!", "Esto es una alerta!", [
      {
        text: "Cancelar",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      { text: "Vale", onPress: () => console.log("OK Pressed") },
    ]);
  return (
    <View style={styles.container}>
            <Text>Buenos días!!</Text>
      <Button title={"Pulsa aquí"} onPress={createTwoButtonAlert} />
      <StatusBar hidden />
      <Button
        title="ModalView"
        onPress={() => navigation.navigate("ModalView", {})}
      />
      <Button title="Check" onPress={() => navigation.navigate("Check", {})} />
      <Button
        title="Calculadora"
        onPress={() => navigation.navigate("Calculadora", {})}
      />
      </View>
  );
}

export function EjemploInicial() {
  console.log("Iniciando aplicación");
  return (
    <View>
      <Text>Ejemplo inicial</Text>
      <Example />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "teal",
  },
});
