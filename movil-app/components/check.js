
import React, { useState } from "react";
import { View, Switch, Text} from "react-native";
const Check = () => {
  const [isEnabled, setIsEnabled] =
    React.useState(false);
  return (
    <View>
        <Text>{isEnabled ? "Encendido": "Apagado"}</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
        value={isEnabled}
        onValueChange={() =>
          setIsEnabled(
            (previousState) => !previousState
          )
        }
      />
    </View>
  );
};

export default Check;
