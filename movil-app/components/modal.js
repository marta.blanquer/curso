import React, { useState } from "react";
import { StyleSheet, Text, View, Modal, Button, Vibration } from "react-native";
const ModalView = ({ texto, textoBoton, textoCerrar }) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  return (
    <View style = {styles.container}>
      <Modal animationType="slide" transparent visible={modalVisible}>
        <View>
          <View style = {styles.modal}>
            <Text style = {styles.text}>{texto}</Text>
            <Button
              title={textoCerrar ? textoCerrar : "Cerrar"}
              onPress={() => { Vibration.vibrate(); setModalVisible(!modalVisible)}}
            />
          </View>
        </View>
      </Modal>
      <Button title={textoBoton ? textoBoton : "Modal"} onPress={() => setModalVisible(true)} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  
  },
  modal: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#00BCD4",
    height: 300,
    width: "80%",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff",
    marginTop: 80,
    marginLeft: 40,
  },
  text: {
    color: "#3f2949",
    marginTop: 10,
  },
});

export default ModalView;
