import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, Button } from "react-native";
import Demos from "./components/demos.js";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
const Stack = createNativeStackNavigator();
import ListaPersonas from "./components/listapersonas";
import Calculadora from "./components/calculadora";
import Check from "./components/check";
import ModalView from "./components/modal";

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Demos" component={Demos} />
        <Stack.Screen name="ModalView" component={ModalView} />
        <Stack.Screen name="Check" component={Check} />
        <Stack.Screen name="Calculadora" component={Calculadora} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
