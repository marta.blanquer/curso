const Calculadora = require("./calculadora");

describe("Pruebas de la calculadora", () => {
  describe("Pruebas de la suma", () => {
    it("Suma de enteros", () => {
      let calc = new Calculadora();
      let resultado = calc.suma(2, 3);
      expect(resultado);
    });
    it("Suma de raros", () => {
      let calc = new Calculadora();
      let resultado = calc.suma(0.1, 0.2);
      expect(resultado);
    });
    beforeEach(() => {
      calculadora = new Calculadora();
    });
    describe.each([
      [1, 1, 2],
      [1, 2, 3],
      [2, 1, 3],
    ])("suma(%i, %i)", (a, b, expected) => {
      test(`returns ${expected}`, () =>
        expect(calculadora.suma(a, b)).toBe(
          parseFloat(expected).toPrecision(15)
        ));
    });
  });
});
describe("Pruebas de la división", () => {});
describe("Dobles de prueba", () => {
  function queLlama(num, fn) {
    return fn(num) + 5;
    //return 45
  }
  it("Crear función", () => {
    const mock = jest.fn((x) => x + 42);
    //expect(mock(3)).toBe(45);
    expect(queLlama(3, mock)).toBe(50);
    expect(mock.mock.calls.length).toBe(1);
    expect(mock.mock.calls[0][0]).toBe(3);
    expect(mock.mock.results[0].value).toBe(45);
  });
  it("Con valores predefinidos", () => {
    const myMock = jest.fn();
    myMock
      .mockReturnValue(4)
      .mockReturnValueOnce(false)
      .mockReturnValueOnce(true);
    expect(myMock()).toBeFalsy();
    expect(myMock()).toBeTruthy();
    expect(myMock()).toBe(4);
  });
  it("promesa resuelta", async () => {
    const asyncMock = jest.fn().mockResolvedValue(43);
    let rslt = await asyncMock(); //43
    expect(rslt).toBe(43);
  });

  it("promesa rechazada", async () => {
    const asyncMock = jest.fn().mockRejectedValue(new Error("fallo"));
    try {
      let rslt = await asyncMock(); //43
      // fail('Tenía que haber fallado')
    } catch (err) {
      expect(err.message).toBe("fallo");
    }
  });
  it("mock suma", async () => {
    const spy = jest.spyOn(calculadora, "suma");
    spy.mockReturnValue(5);
    expect(calculadora.suma(2, 2)).toBe(5);
    expect(spy).toBeCalled();
  });
  it("mock resta", async () => {
    const spy = jest.spyOn(calculadora, "resta");
    spy.mockReturnValue(5);
    expect(calculadora.resta(2, 2)).toBe(5);
    expect(spy).toBeCalled();
  });
  it("mock producto", async () => {
    calculadora.multiplica = jest.fn();
    const spy = jest.spyOn(calculadora, "multiplica");
    spy.mockReturnValue(7);
    expect(calculadora.multiplica(2, 2)).toBe(7);
    expect(spy).toBeCalled();
  });


});
