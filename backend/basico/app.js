const http = require("http");

const hostname = "127.0.0.1";
const port = process.env.port || 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  res.end("Adiós mundo. \n");
});

server.listen(port, hostname, () => {
  console.log(`Server running a http://${hostname}: ${port}/`);
});

/* let nombre = 'mundo'; 

console.log(`Adiós ${nombre}`);
console.log('Mi %s tiene %d patas' , 'gato' , 4);
console.log(port, nombre);

const readline = require('readline').createInterface({
    input: process.stdin, output: process.stdout,
    });
    readline.question(`¿Como te llamas? `, (erro, data) => {
    console.log(`Hola ${data}`);
    readline.close();
    }); 


const circulo = require('./modulos/cuadrado');
console.log(circulo);
const Cuadrado = require('./modulos/cuadrado');
console.log(Cuadrado);
let cuadrado = new Cuadrado(7);
console.log(cuadrado);

const fs = require('fs');

fs.readFile('./modulos/circulo.js', (err, data) => {
    if (err) throw err;
    console.log("leido curculojs")
    });


console.log('sigo')    



console.log('sigo2')  

const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter();
function eventControler(msg) {
    console.log(`EXTRA: ${msg}`)
}

myEmitter.on('pinta', msg => console.log(`NORMAL: ${msg}`))
myEmitter.on('pinta', eventControler)

myEmitter.emit('pinta', 'uno'); */

var mysql = require("mysql");
var connection = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "root",
  database: "sakila",
});

connection.connect();

connection.query(
  "SELECT * FROM `sakila`.`actor`",
  function (err, rows, fields) {
    if (err) throw err;
    rows.forEach((item) => {
      console.log(`${item.actor_id}\t${item.first_name}`);
    });
    console.log(
      JSON.stringify(
        rows.map((item) => ({ id: item.actor_id, nombre: item.first_name }))
      )
    );
  }
);

connection.query(`UPDATE sakila.actor SET first_name = 'PACO' where actor_id = 3`);
