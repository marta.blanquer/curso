const bcrypt = require('bcrypt');
const saltRounds = 10;
async function encriptaPassword(password) {
  const salt = await bcrypt.genSalt(saltRounds);
  const hash = await bcrypt.hash(password, salt);
  console.log(password + ' ' + hash);
  return hash;
}

async function verificaPassword(password, hash) {
  const rslt = await bcrypt.compare(password, hash);
  console.log(password + ' '  + ' ' + hash + ' ' + rslt);
}

encriptaPassword('paco3ertgf4y5h45bj45nuj45j74');
encriptaPassword('ee');
encriptaPassword('pa678co');
encriptaPassword('pacrrrro');
encriptaPassword('Jaime96!');


verificaPassword('paco', '$2b$10$NAk8xx3QLg8soKO33eff3uDtbHeC17swK.gVkxcMonZf86S9bSUdi');
verificaPassword('ee', '$2b$10$0nwty8YONdW5aJ8MozHEaeckZP59stFypnhc9B5Xej2kzYQE6rsXW'); 
verificaPassword('jaime96', '$2b$10$L57oMjXWlBrZjbGi2IAH/O7YxZJ33NZIbFSCdGNzV5lMUureh/Kvm'); 