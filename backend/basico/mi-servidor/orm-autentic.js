const { Sequelize, DataTypes, Op, QueryTypes } = require("sequelize");
const initModels = require("./models/init-models-autentic");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/autenticacion"
);
const dbContext = initModels(sequelize);

console.log(`Inicio ${new Date().toLocaleTimeString("es")}`);

async function allUsers() {
  let rows = await dbContext.usuarios.findAll();
  rows.forEach((row) => {
    console.log(row.toJSON());
  });
}

async function findUserById(mail) {
  let item = await dbContext.usuarios.findOne({
    where: { idUsuario: { [Op.eq]: mail } },
  });

  if (item) {
    console.log(item.toJSON());
  } else {
    console.log("Usuario " + mail + " no encontrado.");
  }
}

let harry = {
  idUsuario: "harryPotter@gmail.com",
  password: "HP1994",
  nombre: "Harry",
  roles: "MAGO",
};

async function insertOne(newUser) {
  try {
    let item = await dbContext.usuarios.create(newUser);
    await item.save();
  } catch {
    console.log(
      "No ha sido posible insertar al usuario " + newUser.idUsuario + "."
    );
  }
}

allUsers();
findUserById("juan@gmail.com");
findUserById("manolo@gmail.com");
findUserById("pepa@gmail.com");

let tallie = {
  idUsuario: "tmewburn2@wikispaces.com",
  password: "hJJYhUh",
  nombre: "Tallie",
  roles: "Occupational Therapist",
};



async function insertMany(usersList) {
  usersList.forEach((row) => {
    insertOne(row);
  });
}

let listaUsuarios = [
  {
    idUsuario: "amonro0@foxnews.com",
    password: "ZMMZ8fxy",
    nombre: "Alard",
    roles: "Project Manager",
  },
  {
    idUsuario: "mgiddy1@elegantthemes.com",
    password: "GH7sdjwTg",
    nombre: "Modesty",
    roles: "Senior Cost Accountant",
  },
  {
    idUsuario: "tmewburn2@wikispaces.com",
    password: "hJJYhUh",
    nombre: "Tallie",
    roles: "Occupational Therapist",
  },
  {
    idUsuario: "ccastano3@time.com",
    password: "3VZRIaF",
    nombre: "Cordelie",
    roles: "Sales Associate",
  },
  {
    idUsuario: "jgeorgeou4@devhub.com",
    password: "VNUVK8i4H",
    nombre: "Joachim",
    roles: "Quality Engineer",
  },
  {
    idUsuario: "kommundsen5@ovh.net",
    password: "Sdp0IvfIw9x",
    nombre: "Kermie",
    roles: "Data Coordiator",
  },
  {
    idUsuario: "rgoodge6@oaic.gov.au",
    password: "Hwojs7pk2R",
    nombre: "Rhodia",
    roles: "VP Accounting",
  },
  {
    idUsuario: "apelham7@163.com",
    password: "JlbYjYV5",
    nombre: "Ardine",
    roles: "Accounting Assistant III",
  },
  {
    idUsuario: "ddenne8@51.la",
    password: "2762kxqEmu",
    nombre: "Dannie",
    roles: "Engineer III",
  },
  {
    idUsuario: "spittman9@github.io",
    password: "chV3oPiy",
    nombre: "Shara",
    roles: "Mechanical Systems Engineer",
  },
];


insertOne(tallie);
