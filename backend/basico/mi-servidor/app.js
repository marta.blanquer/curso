const bcrypt = require('bcrypt');
const saltRounds = 10;


var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var actoresRouter = require("./routes/actores");


var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/api/actores", actoresRouter);
app.use("/demos", require("./routes/demos"));
app.use('/', require('./routes/index'));
app.use('/', require('./routes/seguridad'));
app.use('/users', require('./routes/users'));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;



/* 
async function encriptaPassword(password) {
  const salt = await bcrypt.genSalt(saltRounds);
  const hash = await bcrypt.hash(password, salt);
  console.log(password + ' ' + hash);
  return hash;
}

async function verificaPassword(password, hash) {
  const rslt = await bcrypt.compare(password, hash);
  console.log(password + ' '  + ' ' + hash + ' ' + rslt);
}

encriptaPassword('paco3ertgf4y5h45bj45nuj45j74');
encriptaPassword('ee');
encriptaPassword('pa678co');
encriptaPassword('pacrrrro');


verificaPassword('paco', '$2b$10$NAk8xx3QLg8soKO33eff3uDtbHeC17swK.gVkxcMonZf86S9bSUdi');
verificaPassword('ee', '$2b$10$0nwty8YONdW5aJ8MozHEaeckZP59stFypnhc9B5Xej2kzYQE6rsXW'); */


const jwt = require('jsonwebtoken');
const APP_SECRET = 'Es segura al 99%';
const AUTHENTICATION_SCHEME = 'Bearer';

let token = AUTHENTICATION_SCHEME + jwt.sign({
  usr:'admin',
  name: 'manolo',
  rol: 'jefe'
}, APP_SECRET, {expiresIn: '1h'});

console.log(token);
