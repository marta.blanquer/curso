var DataTypes = require("sequelize").DataTypes;
var _roles = require("./roles");
var _rolesUsuarios = require("./rolesUsuarios");
var _usuarios = require("./usuarios");

function initModels(sequelize) {
  var roles = _roles(sequelize, DataTypes);
  var rolesUsuarios = _rolesUsuarios(sequelize, DataTypes);
  var usuarios = _usuarios(sequelize, DataTypes);

  roles.belongsToMany(usuarios, { as: 'idUsuario_usuarios', through: rolesUsuarios, foreignKey: "idRoles", otherKey: "idUsuario" });
  usuarios.belongsToMany(roles, { as: 'idRoles_roles', through: rolesUsuarios, foreignKey: "idUsuario", otherKey: "idRoles" });
  rolesUsuarios.belongsTo(roles, { as: "idRoles_role", foreignKey: "idRoles"});
  roles.hasMany(rolesUsuarios, { as: "rolesUsuarios", foreignKey: "idRoles"});
  rolesUsuarios.belongsTo(usuarios, { as: "idUsuario_usuario", foreignKey: "idUsuario"});
  usuarios.hasMany(rolesUsuarios, { as: "rolesUsuarios", foreignKey: "idUsuario"});

  return {
    roles,
    rolesUsuarios,
    usuarios,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
