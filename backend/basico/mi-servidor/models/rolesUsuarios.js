const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rolesUsuarios', {
    idUsuario: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'usuarios',
        key: 'idUsuario'
      }
    },
    idRoles: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'roles',
        key: 'idRoles'
      }
    }
  }, {
    sequelize,
    tableName: 'rolesUsuarios',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "idUsuario" },
          { name: "idRoles" },
        ]
      },
      {
        name: "idRoles_idx",
        using: "BTREE",
        fields: [
          { name: "idRoles" },
        ]
      },
    ]
  });
};
