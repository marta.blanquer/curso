let patronDNI = /^\d{8}[A-Z]$/;
let patronNIFX = /^X\d{7}[A-Z]$/;
let patronNIFY = /^Y\d{7}[A-Z]$/;
let patronNIFZ = /^Z\d{7}[A-Z]$/;

let arrayPatrones = [patronDNI, patronNIFX, patronNIFY, patronNIFZ];

function comprobarDigitoControl(nif) {
  let numeros = nif.slice(0, -1);
  if (!numeros.match(/^d/)) {
    numeros = numeros.replace('X', '0').replace('Y', '1').replace('Z', '2');
  }
  let numero = numeros % 23;
  let letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
  letra = letra.substring(numero, numero + 1);

  if (letra != nif.slice(-1)) {
    return false;
  } else {
    return true;
  }
}

let input = prompt('dame un NIF', '');
let match = false;

for (var pat of arrayPatrones) {
  if (input.match(pat)) {
    match = comprobarDigitoControl(input);
  }
}

if (!match) {
  console.log('el NIF no es válido');
  window.alert('el NIF no es válido');
} else {
  console.log('el NIF es válido');
  window.alert('el NIF es válido');
}
