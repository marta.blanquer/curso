import calculaAleatorio from './aleatorio';

let n = calculaAleatorio(100);
window.alert(`el número secreto es ${n}`);
console.log(`el número secreto es ${n}`);

let guess;

for (let i = 0; i < 10; i++) {
  guess = prompt('adivina el entero');
  if (
    isNaN(guess) ||
    guess.trim() == '' ||
    Math.floor(+guess) != +guess ||
    +guess < 0
  ) {
    window.alert('eso no es un entero positivo');
    continue;
  } else if (+guess > n) {
    window.alert('te has pasado');
    continue;
  } else if (+guess < n) {
    window.alert('te has quedado corto');
    console.log(guess);
    continue;
  } else if (guess == n) {
    window.alert('has acertado!!');
    break;
  }
}

function juega(n, guess, win = 'false') {
  let mensaje = '';
  if (
    isNaN(guess) ||
    guess.trim() == '' ||
    Math.floor(+guess) != +guess ||
    +guess < 0
  ) {
    mensaje = 'eso no es un entero positivo';
  } else if (+guess > n) {
    mensaje = 'te has pasado';
  } else if (+guess < n) {
    mensaje = 'te has quedado corto';
  } else if (guess == n) {
    win = true;
    mensaje = 'has acertado!!';
  }
  return [mensaje, win];
}

export default juega;
