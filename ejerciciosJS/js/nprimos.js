

function is_prime(n) {
  if (n < 3) return n > 1;
  else if (n % 2 === 0 || n % 3 === 0) return false;
  else if (n < 25) return true;
  let i = 5;
  while (i * i <= n) {
    if (n % i === 0 || n % (i + 2) === 0) return false;
    i += 6;
  }
  return true;
}

let m = prompt('cuántos primos quieres?');
let resultado = [];
let j = 1;

while (resultado.length < m) {
  if (is_prime(j)) {
    resultado.push(j);
  }
  j++;
}
console.log(resultado);
console.log(j);
