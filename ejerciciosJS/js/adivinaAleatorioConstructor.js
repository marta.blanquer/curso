function calculaAleatorio(hasta) {
  console.log(`número entero aleatorio entre 0 y ${hasta}`);
  var a = Math.random();
  var n = Math.floor(a * (+hasta + 1));
  console.log(n);
  return n;
}

function adivinaAleatorio(hasta, numeroIntentos) {
  let self = this;
  self.hasta = hasta;
  self.numeroIntentos = numeroIntentos;
  self.numeroSecreto = calculaAleatorio(self.hasta);
  self.mensaje = '';
  self.ganador = false;
  self.juega = function (numero) {
      if(self.ganador || self.numeroIntentos <= 0) 
      throw new Error ('juego terminado');
    if (
      isNaN(+numero) ||
      +numero < 0
    ) {
      self.mensaje = 'eso no es un entero positivo';
    } else if (+numero > self.numeroSecreto) {
      self.numeroIntentos--;
      self.mensaje = 'te has pasado';
    } else if (+numero < self.numeroSecreto) {
      self.numeroIntentos--;
      self.mensaje = 'te has quedado corto';
    } else if(+numero == self.numeroSecreto) {
      self.ganador = true;
      self.mensaje = 'has acertado!!';
    }
    return [self.mensaje, self.ganador];
  };
}

let maxIndex = prompt('cuántos intentos quieres?');
let max = prompt('hasta qué número?');

console.log(`número de intentos: ${maxIndex}`);
console.log(`hasta: ${max}`);

let juego1 = new adivinaAleatorio(max, maxIndex);
console.log(juego1);
while (!juego1.ganador && juego1.numeroIntentos > 0) {
  let numeroLeido = prompt('adivina el número');
  juego1.juega(numeroLeido);
  window.alert(`${juego1.mensaje}`);
  console.log(juego1.mensaje);
}

if (!juego1.ganador) {
  window.alert(
    `se te han acabado los intentos, el número secreto era ${juego1.numeroSecreto}`
  );
  console.log(
    `se te han acabado los intentos, el número secreto era ${juego1.numeroSecreto}`
  );
}
