const teclas = document.querySelectorAll(".numero, .operacion, .coma");
const salida = document.querySelector(".pantalla output");
const clear = document.querySelector(".clear");
const igual = document.querySelector(".igual");

let calculadora1 = new calculadora(
  teclas,
  salida,
  clear,
  igual,
  (valor) => (salida.textContent = valor)
);

for (tecla of teclas) {
  tecla.addEventListener("click", (ev) => {
    calculadora1.escribir(salida.textContent, ev.target.dataset.value);
  });
}
clear.addEventListener("click", calculadora1.borrar);
igual.addEventListener("click", (ev) => {
  calculadora1.calcular(salida.textContent);
});

function calculadora(teclas, salida, clear, igual, funcPintar) {
  let cal = this;
  cal.teclas = teclas;
  cal.salida = salida;
  cal.clear = clear;
  cal.igual = igual;
  cal.pintar = funcPintar;

  cal.escribir = function (actual, input) {
    cal.pintar(actual + input);
  };

  cal.borrar = function () {
    cal.pintar("");
  };

  cal.calcular = function (actual) {
    cal.pintar(eval(actual));
  };
}
