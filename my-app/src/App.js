import "./App.css";
import store, { getValueContador } from "./my-store"
import ComponenteContador from "./componenteContador"
import Galeria from "./galeria.js";
import Formulario from "./formulario.js";
import Calculadora from "./calculadora.js";
import ContactosMnt from "./contactos.js";
import PersonasRoute from "./personas.js";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import { ErrorBoundary } from './comunes';
import { render } from "@testing-library/react";
export default function App() {
  return ( 
    <BrowserRouter>
      <nav>

        <ComponenteContador/>
        <Link to='/calculadora/de/marta'>Calculadora</Link> &nbsp;|
        <Link to='/galeria'>Galería</Link> &nbsp;|
        <Link to='/contactos'>Contactos</Link> &nbsp;| 
        <Link to='/personas'>Personas</Link> &nbsp;|
        <Link to='/noexiste'>Mala</Link>
      </nav>
      <ErrorBoundary>
      <Routes>
        <Route path="calculadora/de/marta" element={<ErrorBoundary><Calculadora /></ErrorBoundary>}></Route>
        <Route path="galeria" element={<Galeria numero={200} />}></Route>
        <Route path="contactos" element={<ContactosMnt/>}></Route>
        <Route path="personas/*" element={<PersonasRoute/>}></Route>
        <Route path="*" element={<PageNotFound/>}></Route>
      </Routes>

      </ErrorBoundary>

    </BrowserRouter>
  );
}

function PageNotFound() {
  return (<div> <h1>No se encuentra la página</h1> </div>);
  
}



