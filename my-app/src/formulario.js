import React from "react";
import axios from "axios";

export default class Formulario extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      elemento: {
        id: 0,
        email: "",
        nombre: "",
        contraseña: "",
        recontraseña: "",
        nif: "",
      },
      esInvalido: false,
      errores: {},
    };
    this.form = React.createRef();
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    let cmp = event.target.name;
    let valor = event.target.value;
    let elemento = this.state.elemento;
    elemento[cmp] = valor;
    this.setState((_) => {
      return elemento;
    });
    this.validate();
  }

  validate() {
    if (this.form) {
      let esInvalido = false;
      let errores = {};
      for (let cntr of this.form.current.elements) {
        if (cntr.name) {
          // eslint-disable-next-line default-case
          switch (cntr.name) {
            case "nif":
              if (cntr.value && !esNIF(cntr.value))
                cntr.setCustomValidity("No es un NIF valido");
              else cntr.setCustomValidity("");
              break;
          }
          errores[cntr.name] = cntr.validationMessage;
          if (!cntr.checkValidity()) esInvalido = true;
        }
      }
      this.setState({ errores, esInvalido });
    }
  }

  componentDidMount() {
    this.validate();
  }

  render() {
    return (
      <form ref={this.form}>
        <p>
          <label htmlFor="id">código: </label>
          <input
            type="number"
            id="id"
            name="id"
            value={this.state.elemento.id}
            onChange={this.handleChange}
          />
          <ValidationMessage msg={this.state.errores.id} />
          <br />

          <label htmlFor="email">email: </label>
          <input
            type="string"
            id="email"
            name="email"
            value={this.state.elemento.email}
            onChange={this.handleChange}
          />
          <ValidationMessage msg={this.state.errores.email} />
          <br />

          <label htmlFor="id">nombre: </label>
          <input
            type="string"
            id="nombre"
            name="nombre"
            value={this.state.elemento.nombre}
            onChange={this.handleChange}
            minLength={2}
          />
          <ValidationMessage msg={this.state.errores.nombre} />
          <br />

          <label htmlFor="contraseña">contraseña: </label>
          <input
            type="string"
            id="contraseña"
            name="contraseña"
            value={this.state.elemento.contraseña}
            onChange={this.handleChange}
            required
          />
          <ValidationMessage msg={this.state.errores.contraseña} />
          <br />

          <label htmlFor="recontraseña">recontraseña: </label>
          <input
            type="string"
            id="recontraseña"
            name="recontraseña"
            value={this.state.elemento.recontraseña}
            onChange={this.handleChange}
            required
          />
          <ValidationMessage msg={this.state.errores.recontraseña} />
          <br />

          <label htmlFor="nif">nif: </label>
          <input
            type="string"
            id="nif"
            name="nif"
            value={this.state.elemento.nif}
            onChange={this.handleChange}
          />
          <ValidationMessage msg={this.state.errores.nif} />
          <br />
        </p>
        <p>
          <input
            type="button"
            value="enviar"
            disabled={this.state.esInvalido}
            onClick={() => {
              let elemento = this.state.elemento;
              delete elemento.recontraseña;
              alert(JSON.stringify(elemento));
            }}
          />
          <input type="button" value="contactos" onClick={this.getContactos} />
          <input
            type="button"
            value="cargar"
            onClick={() =>
              this.setState({
                elemento: {
                  id: 99,
                  email: "p@g",
                  nombre: "Pepito",
                  contraseña: "kkkk",
                  recontraseña: "kkkk",
                  nif: "4g",
                },
              })
            }
          />
        </p>
      </form>
    );
  }
}

class ValidationMessage extends React.Component {
  render() {
    if (this.props.msg) {
      return (
        <span style={{ color: "red" }} className="errorMsg ">
          {this.props.msg}{" "}
        </span>
      );
    }
    return null;
  }
}

function esNIF(nif) {
  if (!/^\d{1,8}[A-Za-z]$/.test(nif)) return false;
  const letterValue = nif.substr(nif.length - 1).toUpperCase();
  const numberValue = nif.substr(0, nif.length - 1);
  return letterValue === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(numberValue % 23);
}
