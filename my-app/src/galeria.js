import React from "react";
import "./estilo/galeria.css";

export class Foto extends React.Component {
  constructor(props) {
    super(props);
    let { id, zoom} = props;
    this.id = id;
    this.state = {
      hidden: true,
    };
    this.zoom = zoom;
    this.descargar = this.descargar.bind(this);
  }
  shouldComponentUpdate(next_props) {
    this.zoom = next_props.zoom;
    return true;

  }
  descargar() {
    this.setState((_) => {
      return { hidden: false };
    });
  }
  render() {
    let base = "https://picsum.photos/id/";
    if (this.state.hidden) {
      return (
        <>
          <div className="marco" onClick={this.descargar} style={{cursor: 'pointer'}}>
            <div className="hueco">{this.id+1}</div>
            <div>{this.zoom >= 0.8 ? 'Click para descargar la imagen' : ''}</div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="marco">
            <img
              src={base + this.id + "/200/300"}
              alt="texto alternativo"
            ></img>
            <div>{this.zoom >= 0.8 ? 'Imagen descargada desde: ' + base + this.id + "/" : ''} </div>
          </div>
        </>
      );
    }
  }
}

export default class Galeria extends React.Component {
  constructor(props) {
    super(props);
    let { numero } = props;
    this.numero = numero;
    this.state = {
      zoom: 1,
      
    };
    this.mas = this.mas.bind(this);
    this.menos = this.menos.bind(this);
  }

  mas() {
    this.setState((prev) => {
      return { zoom: +prev.zoom+0.1 };
    });
  }

  menos() {
    this.setState((prev) => {
      return { zoom: +prev.zoom-0.1 };
    });
  }

  render() {
    let fotos = [];
    for (let n = 0; n < this.numero; n++) {
      fotos.push(<Foto id={n} key={n} zoom={this.state.zoom}></Foto>);
    }
    return (
      <>
        <div className="controles">
        <button onClick={this.mas}>+</button>&nbsp;
        <button onClick={this.menos}>-</button>
        </div>
        <div className="galeria" style={{ zoom: this.state.zoom }}>
          {fotos}
        </div>
      </>
    );
  }
}
