import React from "react";
import "./estilo/calculadora.css";
import { ErrorBoundary } from './comunes';

export default class Calculadora extends React.Component {
  constructor(props) {
    super(props);
    let { inicio } = props;
    this.state = {
      pantalla: inicio,
    };

    this.escribir = this.escribir.bind(this);
    this.borrar = this.borrar.bind(this);
    this.calcular = this.calcular.bind(this);
  }
  escribir(e) {
    const tecla = e.currentTarget.dataset.value;
    console.log(tecla);
    this.setState((prev, props) => {
      const cont = prev.pantalla + tecla;
      return { pantalla: cont };
    });
  }

  borrar(e) {
    this.setState((prev, props) => {
      return { pantalla: "" };
    });
  }

  calcular(e) {
    this.setState((prev, props) => {
      return { pantalla: eval(prev.pantalla) };
    });
  }

  render() {
    return (
      <>
        <ErrorBoundary>

        
        <h1>La calculadora de Marta</h1>

        <div className="pantalla">
          <output>{this.state.pantalla}</output>
        </div>

        <div className="controles">
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="0"
          >
            0
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="1"
          >
            1
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="2"
          >
            2
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="3"
          >
            3
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="4"
          >
            4
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="5"
          >
            5
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="6"
          >
            6
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="7"
          >
            7
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="8"
          >
            8
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="numero"
            data-value="9"
          >
            9
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="operacion"
            data-value="+"
          >
            +
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="operacion"
            data-value="-"
          >
            -
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="operacion"
            data-value="/"
          >
            /
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="operacion"
            data-value="*"
          >
            *
          </div>
          <div
            onClick={this.escribir}
            role="button"
            className="coma"
            data-value="."
          >
            .
          </div>
          <div onClick={this.borrar} role="button" className="clear">
            C
          </div>
          <div
            onClick={this.calcular}
            role="button"
            className="igual"
            data-value="="
          >
            =
          </div>
        </div>
        </ErrorBoundary>
      </>
    );
  }
}
