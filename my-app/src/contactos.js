import React, { Component } from "react";
import axios from "axios";
import { ValidationMessage, Esperando } from "./comunes";
import { ErrorBoundary } from './comunes';
import store from './my-store';
// import * as MyStore from "./my-store";

export default class ContactosMnt extends Component {
  constructor(props) {
    super(props);
    let pagina = props?.match?.params?.page ? props.match.params.page : 0;
    this.state = { 
      modo: "list", 
      listado: [], 
      elemento: null, 
      loading: true, 
      pagina 
    };
    this.idOriginal = null;
    this.url = "http://localhost:4321/api/contactos";
    this.mas = this.mas.bind(this);
    this.menos = this.menos.bind(this); 
  }

  mas() {
    this.setState((prev) => {
      return { pagina: +prev.pagina+1 < prev.paginas ? +prev.pagina+1 : prev.paginas-1 };
    });
    this.list();
  }

  menos() {
    this.setState((prev) => {
      return { pagina: +prev.pagina-1 >= 0 ? +prev.pagina-1 : 0};
    });
    this.list();
  }

  list() {
    this.setState({ loading: true });
    axios
      .get(`${this.url}?_page=count&_rows=10`)
      .then(resp => {
        let paginas = resp.data.pages;
        let pagina = this.state.pagina;
        this.setState({ pagina, paginas });
        axios
          .get(`${this.url}?_page=${pagina}&_rows=10`)
          .then(resp => {
            this.setState({
              modo: "list",
              listado: resp.data,
              loading: false
            });
          })
          .catch(err => {
            // MyStore.AddErrNotifyCmd(err);
            this.setState({ loading: false });
          });
      }).catch(err => {
        // MyStore.AddErrNotifyCmd(err);
        this.setState({ loading: false });
      });

    // axios
    //   .get(`${this.url}?page=${this.state.pagina}`)
    //   .then(resp => {
    //     this.setState({
    //       modo: "list",
    //       listado: resp.data,
    //       loading: false
    //     });
    //   })
    //   .catch(err => {
    //     MyStore.AddErrNotifyCmd(err);
    //     this.setState({ loading: false });
    //   });
  }
  add() {
    this.setState({
      modo: "add",
      elemento: { id: "", tratamiento: "", nombre: "", apellidos: "", telefono: "", email: "", sexo: "", nacimiento: "", avatar:"", conflictivo: "" }
    });
  }
  edit(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "edit",
          elemento: resp.data,
          loading: false
        });
        this.idOriginal = key;
      })
      .catch(err => {
        // MyStore.AddErrNotifyCmd(err);
        this.setState({ loading: false });
      });
  }
  view(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "view",
          elemento: resp.data,
          loading: false
        });
      })
      .catch(err => {
        // MyStore.AddErrNotifyCmd(err);
        this.setState({ loading: false });
      });
  }
  delete(key) {
    if (!window.confirm("¿Seguro?")) return;
    this.setState({ loading: true });
    axios
      .delete(this.url + `/${key}`)
      .then(resp => this.list())
      .catch(err => {
        // MyStore.AddErrNotifyCmd(err);
        this.setState({ loading: false });
      });
  }
  cancel() {
    this.list();
  }
  send(elemento) {
    // eslint-disable-next-line default-case
    switch (this.state.modo) {
      case "add":
        axios
          .post(this.url, elemento)
          .then(data => this.cancel())
          .catch(err => {
            // MyStore.AddErrNotifyCmd(err);
            this.setState({ loading: false });
          });
        break;
      case "edit":
        axios
          .put(this.url + `/${this.idOriginal}`, elemento)
          .then(data => this.cancel())
          .catch(err => {
            // MyStore.AddErrNotifyCmd(err);
            this.setState({ loading: false });
          });
        break;
    }
  }
  decodeRuta() {
    // if (this.props.match.url === this.urlActual) return;
    // this.urlActual = this.props.match.url;
    // if (this.props.match.params.id) {
    //   if (this.props.match.url.endsWith('/edit'))
    //     this.edit(this.props.match.params.id);
    //   else
    //     this.view(this.props.match.params.id);
    // } else {
    //   if (this.props.match.url.endsWith('/add'))
    //     this.add();
    //   else
    //     this.list();
    // }
  }

  componentDidMount() {
    this.list();
    // this.decodeRuta();
  }
  componentDidUpdate() {
    // this.decodeRuta();
  }

  render() {
    if (this.state.loading) return <Esperando />;
    switch (this.state.modo) {
      case "add":
      case "edit":
        return (
          <ErrorBoundary>

            <ContactosForm
              isAdd={this.state.modo === "add"}
              elemento={this.state.elemento}
              onCancel={e => this.cancel()}
              onSend={e => this.send(e)}
            />
          </ErrorBoundary>
        );
      case "view":
        return (
          <ContactosView
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
          />
        );
      default:
        return (
          <>
          <div className="paginador" style={{height: "100px"}}> Total páginas: {this.state.paginas} Página:  {this.state.pagina}</div>
          <button onClick={this.mas}>+</button>&nbsp;
          <button onClick={this.menos}>-</button>
          <ContactosList
            listado={this.state.listado}
            onAdd={e => this.add()}
            onView={key => this.view(key)}
            onEdit={key => this.edit(key)}
            onDelete={key => this.delete(key)}
          />
          </>
        );
    }
  }
}

export class ContactosList extends Component {
  render() {
    return (
      <table className="table table-striped">
        <thead className="thead-dark">
          <tr>
            <th>Nombre</th>
            <th>
              <input
                type="button"
                className="btn btn-primary"
                value="Añadir"
                onClick={e => this.props.onAdd()}
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {this.props.listado.map(item => (
            <tr key={item.id}>
              <td>
                {item.id} {item["nombre"]} {item.apellidos}
              </td>
              <td>
                <input
                  type="button"
                  className="btn btn-primary"
                  value="Ver"
                  onClick={e => this.props.onView(item.id)}
                />
                <input
                  type="button"
                  className="btn btn-primary"
                  value="Editar"
                  onClick={e => this.props.onEdit(item.id)}
                />
                <input
                  type="button"
                  className="btn btn-danger"
                  value="Borrar"
                  onClick={e => this.props.onDelete(item.id)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
export class ContactosView extends Component {
  render() {
    return (
      <div>
        <p>
          <b>Código:</b> {this.props.elemento.id}
          <br />
          <b>Tratamiento:</b> {this.props.elemento.tratamiento}
          <br />
          <b>Nombre:</b> {this.props.elemento.nombre}
          <br />
          <b>Apellidos:</b> {this.props.elemento.apellidos}
          <br />
          <b>Teléfono:</b> {this.props.elemento.telefono}
          <br />
          <b>Email:</b> {this.props.elemento.email}
          <br />
          <b>Sexo:</b> {this.props.elemento.sexo}
          <br />
          <b>Nacimiento:</b> {this.props.elemento.nacimiento}
          <br />
          <b>Avatar:</b> <img src={this.props.elemento.avatar} alt="avatar del contacto" style={{width: '100px'}}></img>
          <br />
          <b>Conflictivo:</b> {this.props.elemento.conflictivo ? 'Conflictivo': 'No conflictivo'}
          <br />
        </p>
        <p>
          <button
            className="btn btn-primary"
            type="button"
            onClick={e => this.props.onCancel()}
          >
            Volver
          </button>
        </p>
      </div>
    );
  }
}

export class ContactosForm extends Component {
  constructor(props) {
    super(props);
    this.state = { elemento: props.elemento, msgErr: [], invalid: false };
    this.handleChange = this.handleChange.bind(this);
    this.onSend = () => {
      if (this.props.onSend) this.props.onSend(this.state.elemento);
    };
    this.onCancel = () => {
      if (this.props.onCancel) this.props.onCancel();
    };
  }
  handleChange(event) {
    const cmp = event.target.name;
    const valor = event.target.value;
    this.setState(prev => {
      prev.elemento[cmp] = valor;
      return { elemento: prev.elemento };
    });
    this.validar();
  }
  validarCntr(cntr) {
    if (cntr.name) {
      // eslint-disable-next-line default-case
      switch (cntr.name) {
        case "nombre":
          cntr.setCustomValidity(cntr.value !== cntr.value.toUpperCase()
            ? "Debe estar en mayúsculas" : '');
          break;
      }
    }
  }
  validar() {
    if (this.form) {
      const errors = {};
      let invalid = false;
      for (var cntr of this.form.elements) {
        if (cntr.name) {
          this.validarCntr(cntr);
          errors[cntr.name] = cntr.validationMessage;
          invalid = invalid || !cntr.validity.valid;
        }
      }
      this.setState({ msgErr: errors, invalid: invalid });
    }
  }
  componentDidMount() {
    this.validar();
  }
  render() {
    return (
      <form
        ref={tag => {
          this.form = tag;
        }}
      >
        {this.props.isAdd && (
          <div className="form-group">
            <label htmlFor="id">Código</label>
            <input
              type="number"
              className="form-control"
              id="id"
              name="id"
              value={this.state.elemento.id}
              onChange={this.handleChange}
       
            />
            <ValidationMessage msg={this.state.msgErr.id} />
          </div>
        )}
        {!this.props.isAdd && (
          <div className="form-group">
            <label>Código</label>
            {this.state.elemento.id}
          </div>
        )}
        <div className="form-group">
          <label htmlFor="tratamiento">Tratamiento</label>
          <input
            type="text"
            className="form-control"
            id="tratamiento"
            name="tratamiento"
            value={this.state.elemento.tratamiento}
            onChange={this.handleChange}
            required
            minLength="2"
            maxLength="10"
          />
          <ValidationMessage msg={this.state.msgErr.tratamiento} />
        </div>
        <div className="form-group">
          <label htmlFor="nombre">Nombre</label>
          <input
            type="text"
            className="form-control"
            id="nombre"
            name="nombre"
            value={this.state.elemento.nombre}
            onChange={this.handleChange}
            required
            minLength="2"
            maxLength="10"
          />
          <ValidationMessage msg={this.state.msgErr.nombre} />
        </div>
        <div className="form-group">
          <label htmlFor="apellidos">Apellidos</label>
          <input
            type="text"
            className="form-control"
            id="apellidos"
            name="apellidos"
            value={this.state.elemento.apellidos}
            onChange={this.handleChange}
            minLength="2"
            maxLength="10"
          />
          <ValidationMessage msg={this.state.msgErr.apellidos} />
        </div>
        <div className="form-group">
          <label htmlFor="edad">Teléfono</label>
          <input
            type="text"
            className="form-control"
            id="telefono"
            name="telefono"
            value={this.state.elemento.telefono}
            onChange={this.handleChange}
            minLength="9"
            maxLength="20"
          />
          <ValidationMessage msg={this.state.msgErr.telefono} />
        </div>
        <div className="form-group">
          <label htmlFor="edad">Email</label>
          <input
            type="email"
            className="form-control"
            id="email"
            name="email"
            value={this.state.elemento.email}
            onChange={this.handleChange}
            minLength="3"
            maxLength="250"
          />
          <ValidationMessage msg={this.state.msgErr.email} />
        </div>
        <div className="form-group">
          <button
            className="btn btn-primary"
            type="button"
            disabled={this.state.invalid}
            onClick={this.onSend}
          >
            Enviar
          </button>
          <button
            className="btn btn-primary"
            type="button"
            onClick={this.onCancel}
          >
            Volver
          </button>
        </div>
      </form>
    );
  }
}
