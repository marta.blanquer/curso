/* export const Saluda = (props) => (<h1>Hola {props.nombre}</h1>) */
/* export const Saluda = ({nombre}) => (<h1>Hola {nombre}</h1>) */

import React from "react";
import PropTypes from "prop-types";
import logo from "./logo.svg";

export class Saluda extends React.Component {
  constructor(props) {
    super(props);
    let { nombre, apellidos } = props;
    this.nombre = nombre;
  }
  render() {
    return <h1>Hola {this.nombre}</h1>;
  }
}

Saluda.propTypes = {
  nombre: PropTypes.string.isRequired,
  apellidos: PropTypes.string,
  edad: PropTypes.number,
};

Saluda.defaultProps = {
  nombre: "mundo",
  edad: 99,
};

export const Card = ({ title, children }) => (
  <div className="card">
    <div className="card-body alert-danger">
      <h1 className="card-title">{title}</h1>
      <div className="card-text">{children}</div>
    </div>
  </div>
);

export const Despide = (props) => <h1>Adiós {props.nombre}</h1>;

export class Contador extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contador: +props.init,
      delta: +props.delta,
    };
    this.baja = this.baja.bind(this);
    this.init = this.init.bind(this);

    this.sube = (e) => {
      e.preventDefault();
      this.setState((prev, props) => {
        return { contador: prev.contador + prev.delta };
      });
    };
  }
  baja(e) {
    e.preventDefault();
    this.setState((prev, props) => {
      return { contador: prev.contador - prev.delta };
    });
  }

  init(value, e) {
    e.preventDefault();
    this.setState({ contador: value });
  }

  notifyChange(value) {
    if (this.props.onCambia) {
      this.props.onCambia(value);
    }
  }

  render() {
    return (
      <div>
        <h1>{this.state.contador}</h1>
        <p>
          <button onClick={this.sube}>Sube</button>&nbsp;
          <button onClick={this.baja}>Sube</button>&nbsp;
          <button
            onClick={(ev) => {
              this.baja(ev);
            }}
          >
            Baja
          </button>
          <button
            onClick={(ev) => {
              this.init(0, ev);
            }}
          >
            Init
          </button>
        </p>
      </div>
    );
  }
}

export default function Demos() {
  const element = (
    <>
      <h1>Hola mundo</h1>
      <h2>Qué tal?</h2>
    </>
  );
  function negrita(item) {
    if (item.id % 2) {
      return <b>{item.nombre}</b>;
    } else {
      return <></>;
    }
  }
  let nombre = "Marta";
  let listado = [
    { id: 1, nombre: "Camelo", apellidos: "Coton", edad: 37 },
    { id: 2, nombre: "Pepito", apellidos: "Grillo", edad: 67 },
    { id: 3, nombre: "<b>Pierre</b>", apellidos: "Nodoiuna", edad: 55 },
    { id: 4, nombre: "Capitan", apellidos: "Tan", edad: 18 },
  ];
  return (
    <div className="App">
      <header className="App-header">
        <h1>{process.env.REACT_APP_NOMBRE}</h1>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="btn btn-info"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <h1>Secreto: {process.env.REACT_APP_SECRET}</h1>
      </header>
      <Contador init="0" delta="2"></Contador>
      {element}
      {nombre && (
        <h3 className="alert alert-danger">Eyyy {nombre.toUpperCase()}!!!</h3>
      )}
      <ul>
        {listado.map((item) => (
          <li
            key={item.id}
            className={item.id % 2 ? "alert-danger" : "alert-info"}
          >
            {negrita(item)} {item.apellidos}
            <Saluda {...item}></Saluda>
          </li>
        ))}
      </ul>
      <Card title="Título de la tarjeta">
        <Saluda edad={34}></Saluda>
        <Saluda nombre="Don Jose"></Saluda>
        <Despide></Despide>
        <Despide nombre="Don Jose"></Despide>
      </Card>
    </div>
  );
}
